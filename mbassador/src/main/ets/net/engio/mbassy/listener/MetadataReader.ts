/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Handler, HandlerOptions } from './Handler';
import HashMap from '@ohos.util.HashMap';
import ArrayList from '@ohos.util.ArrayList';
import { References } from './References';
import { MethodParameters } from '../entry/MethodParameters';


export class MetadataReader {
    private static sInstance: MetadataReader;
    private handleMap: HashMap<String, ArrayList<MethodParameters>>;
    private referencesMap: HashMap<String, References>;

    private constructor() {
        this.handleMap = new HashMap<String, ArrayList<MethodParameters>>();
        this.referencesMap = new HashMap<String, References>();
    }

    public static getInstance(): MetadataReader {
        if (!this.sInstance) {
            this.sInstance = new MetadataReader();
        }
        return this.sInstance;
    }

    public setSubHandle(className: String, methodName: String, options: HandlerOptions) {
        if (!className || className.trim().length == 0) {
            throw new Error("the listener no get className to handle");
        }
        let handle = new Handler(options);
        var methodPa: ArrayList<MethodParameters> = new ArrayList();
        if (this.handleMap.hasKey(className)) {
            methodPa = this.handleMap.get(className);
        }

        var isSample: boolean = false;
        if (methodPa.length > 0) {
            methodPa.forEach((value, index) => {
                if (value.getMethodName() == methodName) {
                    isSample = true;
                }
            })
        }
        if (!isSample) {
            methodPa.add(new MethodParameters(methodName, handle));
        }
        this.handleMap.set(className, methodPa);
    }

    public setListenerReferencesType(className: String, reType: References): void {
        if (!reType) {
            return;
        }
        if (!className || className.trim().length == 0) {
            throw new Error('the listener no get className to handle');
        }
        if (this.referencesMap.hasKey(className)) {
            this.referencesMap.remove(className);
        }
        this.referencesMap.set(className, reType);
    }

    public getMethodHandle(className: String): ArrayList<MethodParameters> | undefined {
        if (!className || className.trim().length == 0) {
            return undefined;
        }
        if (this.handleMap.hasKey(className)) {
            return this.handleMap.get(className);
        }
        return undefined;
    }

    getResType(className: String): References {
        if (!className || className.trim().length == 0) {
            return References.Strong;
        }
        if (this.referencesMap.hasKey(className)) {
            return this.referencesMap.get(className);
        }
        return References.Strong;
    }

    useStrongReferences(className: String): boolean {
        let resType = this.getResType(className);
        return!resType ? true : resType == References.Strong.valueOf();
    }
}